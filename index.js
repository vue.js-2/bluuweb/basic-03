const app = new Vue({
  el: "#app",
  data: {
    mensaje: "Hola soy Aurelio Miguel",
    contador: 0,
    isActive: true,
    hasError: true,
    classObject: {
      active: true,
      "text-danger": true,
    },
  },
  computed: {
    invertirMensaje() {
      return this.mensaje.split("").reverse().join("");
    },
    color() {
      return {
        "bg-success": this.contador <= 5,
        "bg-alert": this.contador > 5 && this.contador <= 10,
        "bg-error": this.contador > 10,
      };
    },
  },
});
